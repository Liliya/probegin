from django.contrib.auth.decorators import login_required
from django.shortcuts import render, get_object_or_404, redirect
from django.utils import timezone

from probegin.forms import PostForm, CommentForm
from probegin.serializers import PostSerializer
from .models import Post, Comment


def post_list(request):
    posts = Post.objects.filter(published_date__lte=timezone.now()).order_by('published_date')
    serializer = PostSerializer(posts, many=True)
    return render(request, 'post_list.html', {'posts': serializer.data})


@login_required
def post_detail(request, pk):
    post = Post.objects.prefetch_related('comment_set').get(pk=pk)
    serializer = PostSerializer(post)
    comment = CommentForm()
    return render(request, 'post_detail.html', {'post': serializer.data, 'add_comment': comment})


def post_new(request):
    if request.method == "POST":
        form = PostForm(request.POST)
        if form.is_valid():
            post = form.save(commit=False)
            post.author = request.user
            post.published_date = timezone.now()
            post.save()
            return redirect('post_detail', pk=post.pk)
    else:
        form = PostForm()
    return render(request, 'post_edit.html', {'form': form})


def post_edit(request, pk):
    post = get_object_or_404(Post, pk=pk)
    if request.method == "POST":
        form = PostForm(request.POST, instance=post)
        if form.is_valid():
            post = form.save(commit=False)
            post.author = request.user
            post.published_date = timezone.now()
            post.save()
            return redirect('post_detail', pk=post.pk)
    else:
        form = PostForm(instance=post)
    return render(request, 'post_edit.html', {'form': form})


@login_required
def add_comment(request, pk_post):
    post = get_object_or_404(Post, pk=pk_post)
    if request.method == "POST":
        form = CommentForm(request.POST)
        if form.is_valid():
            comment = form.save(commit=False)
            comment.author = request.user
            comment.post = post
            comment.save()
            return redirect('post_detail', pk=post.pk)
    else:
        form = PostForm(request.POSTs)
    return render(request, 'post_edit.html', {'add_comment': form})
